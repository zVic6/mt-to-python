from sys import path
from os.path import abspath, dirname 

dirname = dirname(abspath(__file__))
new_dir = dirname+'/../python/'
path.append(new_dir)

import parNMPC


from casadi import vertcat
from system_template import base_system_class as bsc
import optimization
import numpy as np

class system_class(bsc):

    def __init__(self): 
        self.m     = 0.5
        self.state_labels = [r'$x_1$',r'$\dot{x_1}$']
        self.input_labels = [r'$u_1$']
        self.output_labels= [r'$y_1$']
        self.x0 = [0,0]
        self.u0 = [0]
        m = 1e2
        self.lbx = [-m, -m]
        self.ubx = [m, m]
        self.lbu = [-1]
        self.ubu = [1 ]
        self.r_time = [[0,4.99,5,20]]
        self.r_data = [[0,0,1 , 1]]

        bsc.test_assertions(self)

    def derivative_model(self,x,u):
        return vertcat(x[1],u[0]/self.m)
    
    def derivative_real(self,x,u):
        # 10% error on parameter
        return vertcat(x[1],u[0]/(self.m*1.1))

    def output(self,x,u):
        return vertcat(x[0])
        
    def noise(self):
        return vertcat(0)

# instantiate the dynamic system
sys = system_class()
# instantiate the parameter class that passes relevant information to the optimization
p = parNMPC.parameter_class(sys)
# make the sampling time Ts and prediction horizon Hp variable parameters
p.add_varpar(name="Ts",values=[0.1,1])
p.add_varpar(name="Hp",values=[5,10])
# instantiate the optimization class
mpc = optimization.optimization()
# pass the parameter instance and run the optimization
usim,xsim,ysim,ymeas = mpc.run(p)

dataFrame = p.print_experiments()
figs,axs = p.plot_experiments()