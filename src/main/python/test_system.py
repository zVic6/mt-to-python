from casadi import vertcat
from system_template import base_system_class as bsc

class system_class(bsc):

    def __init__(self):
        # define the relevant physical constants
        # 
        self.m     = 0.5              # mass [kg]

        # States names and description
        # x1: position
        # x2: speed
        self.state_labels = [r'$x_1$',r'$\dot{x_1}$']

        # Inputs names and description
        # u1: force
        self.input_labels = [r'$u_1$']

        # Outputs names and description
        # y1: position
        self.output_labels= [r'$y_1$']

        # Initial conditions
        self.x0 = [0,0]
        self.u0 = [0]

        # State Bounds
        m = 1e2
        self.lbx = [-m, -m]
        self.ubx = [m, m]

        # Input Bounds
        self.lbu = [-1]
        self.ubu = [1 ]

        # Reference Trajectory
        self.r_time = [[0,9.99,10,20]]
        self.r_data = [[0,0,1 , 1]]

        # Run checks
        bsc.test_assertions(self)

    # model system dynamics
    def derivative_model(self,x,u):
        # xdot = dx/dt(x,u) implementation goes here
        dx1 = x[1]
        dx2 = u[0]/self.m
        return vertcat(dx1,dx2)

    # real system dynamics
    def derivative_real(self,x,u):
        # xdot = dx/dt(x,u) implementation goes here
        dx1 = x[1]
        dx2 = u[0]/(self.m*1.1)
        return vertcat(dx1,dx2)

    # system output
    def output(self,x,u):
        return vertcat(x[0])
        
    # system-specific noise function
    def noise(self):
        return vertcat(0)

    