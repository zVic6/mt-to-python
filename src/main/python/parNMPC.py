from numpy import ceil, linspace, zeros, vstack, tile, ones
import pandas as pd
import importlib
from system_template import base_system_class
from matplotlib import pyplot as plt

# varpar allows to observe the effects of parameter variation in main.py
# the variable is changed via global access.
# 
# The varpar_container stores all instances of varpar.
# varpars are instantiated via container 
class varpar_container(): # pragma: no cover
    finished = False                                    # check for last combination
    index = 0                                           # current varpar index subject to change
    vp_list = []                                        # varpar list. TODO check empty before use
    experiments = []

    def __init__(self,parameter_class_instance, outside_update = None):
        assert parameter_class_instance is not None
        self.pc_instance = parameter_class_instance
        self.outside_update = outside_update
        if outside_update is not None:
            assert callable(outside_update)
        self.update_parameter_class()
    
    def update_parameter_class(self):
        for vp in self.vp_list:
                setattr(vp.ref_to_instance, vp.name, vp.value)
        if self.outside_update is not None:
            self.outside_update()

    # add varpar to the list
    def add_varpar(self,ref_to_instance,name,values):
        vp = self.varpar(ref_to_instance=ref_to_instance,name=name,value_list=values)
        self.vp_list.append(vp)
        self.update_parameter_class()
        
    # recursive routine to change varpar to the next permutation
    # all combinations are tried once
    # returns True, when done
    def iterate(self):
        if len(self.vp_list) > 0:
            _,change = self.vp_list[self.index].iterate()   # change value of varpar[index]
            if change:                                      # last value in varpar reached
                if self.index+1 == len(self.vp_list):       # overall last combination reached
                    self.finished = True
                else:                                       # else recursively adapt next varpars
                    self.index += 1
                    self.iterate()
            self.index = 0                                  # restore index to first varpar instance
        else:
            self.finished = True
        self.update_parameter_class()
        return self.finished

    # print the current permutation
    def print_current(self):
        if len(self.vp_list)>0:
            for vp in self.vp_list:
                print(vp.name,":",vp.value)
        else:
            print("varpar_container.print_current(): No variable parameters in container")

    # return the total number of permutations
    def number_of_permutations(self):
        num_perm = 1
        for vp in self.vp_list:
            num_perm *= len(vp.value_list)
        return num_perm
    
    # return a dictionary with the current parameter configuration
    def get_current(self):
        configuration = {}
        for vp in self.vp_list:
            configuration[vp.name] = vp.value
        return configuration

    # Save the experiment's results of the current parameter configuration
    # 
    # experiments = [dict_1 dict_2 ... dict_numOfpermutations]
    # dict_i = {vp_1_name:vp_1_value,...vp_m_name:vp_m_value, results_i}
    # where m = len(vp_list) and results_i is again a dictionary defined
    # in main.py that stores relevant data
    def store_experiment(self,results,iterate=False):
        configuration = self.get_current()
        for name, value in results.items():
            configuration[name] = value

        self.experiments.append(configuration)
        if iterate:
            self.iterate()

    """
    Can print specific values stored in experiment only, e.g.
    p.VPC.print_experiments(names=['Ts','nsqp'])
    """
    def print_experiments(self,names=[]):
        df = pd.DataFrame(self.experiments)
        #results is a dictionary, contains ALL system signals
        del df['results'] 

        # extract the performance measures for all experiments
        algo_vars = pd.DataFrame([exp['results']['algo_vars'] for exp in self.experiments])
        
        # join & print the dataframes
        df_final = pd.concat([df, algo_vars], axis=1)

        # if given, only select certain key names
        if len(names)>0:
            print(df_final.filter(items=names))
        else:
            print(df_final)
            
        return df_final

    def plot_results(self,signal,show=True,title='',noise=False):
        sig_str = 'ysim'
        if noise:
            sig_str = 'ymeas'

        t,u,y = signal['time'],signal['usim'],signal[sig_str]
        nu,ny = u.shape[0],y.shape[0]
        nsim, numOfPlots = u.shape[1], nu+ny
        
        fig, axs = plt.subplots(numOfPlots)
        axs[0].set_title(title)
        sys = self.pc_instance.sys
        full_ref = sys.reference(t)
        for p_iter in range(ny):
            label = sys.output_labels[p_iter]
            axs[p_iter].step(t, full_ref[p_iter,:],'r--',where='post',label=label+r'$_\mathrm{ref}$')
            axs[p_iter].step(t,y[p_iter,:],          where='post',label=label)
            axs[p_iter].legend()

        for p_iter in range(ny,numOfPlots):
            axs[p_iter].step(t, u[p_iter-ny,:],'--',label=sys.input_labels[p_iter-ny])
            axs[p_iter].legend()
        
        if show:
            plt.show()
        return fig,axs

    def plot_experiments(self,noise = False):
        
        figs = []
        axs  = []
        
        # get names of varpars used for the experiments
        vp_names = [vp.name for vp in self.vp_list]

        for experiment in self.experiments:
            title = [vp_name+" = "+str(experiment[vp_name])+" " for vp_name in vp_names]
            fig,ax = self.plot_results(signal=experiment['results']['signals'],show=False,title=''.join(title),noise=noise)
            figs.append(fig)
            axs.append(ax)

        plt.show()
        return figs,axs
    # -------------------------------- NESTED CLASS varpar -------------------------------------------
    class varpar():
        name = ""                                           # name of variable
        index = 0
        value = 0                                           # current value
        value_list = []                                     # stores parameter variations

        # initialize the varpar instance
        # Note: the 'name' must be identical to the variable name already defined
        # and be given in double quotation marks ("")!
        def __init__(self,ref_to_instance,name,value_list):
            self.ref_to_instance = ref_to_instance
            self.name = name
            self.value_list = value_list
            self.value = self.value_list[0]
            # globals()[self.name] = self.value

        # change value to the next entry in value_list
        def iterate(self):
            reset = True

            if self.index+1 == len(self.value_list):        # check end of list
                self.index = 0                              # reset to 1st element
            else:
                self.index += 1
                reset = False

            self.value = self.value_list[self.index]        # update value
            # globals()[self.name] = self.value               # set actual variable
            return self.value , reset                       # retrn information about reset

    # ----------------------------- EO NESTED CLASS varpar -------------------------------------------
# ----------------------------- EO CLASS varpar_container ----------------------------------------

class parameter_class():
    
    """
    If you want to add a variable parameter for automated experiment comparison
    then you have to add them explicitly in your script after initializing
    your parameter_class instance 'p'. The following are examples:
    
        p.VPC.add_varpar(name="Ts",values=[0.1,0.5,1])
        p.VPC.add_varpar(name="int_name",values=['euler','rk4'])

    """
    # -*-*-*-*-*-*-*-*-*-*-*-*- Adapt the following to your Problem -*-*-*-*-*-*-*-*-*-*-*-*-

    # MPC Parameters
    Ts = 0.1                        # Sampling Time
    Hp = 20                         # Control Horizon (Hp*Ts is control horizon in seconds)
    # TODO: add boolean to switch Hp from steps to seconds. in seconds-mode, the real Hp is then calc.

    # SQP Parameters
    nsqp = 10                        # Number of SQP Iterations
    asqp = 0.1                        # SQP Stepsize
    Jtol = 1e-3                     # Cost Function Upper Bound

    # Integrator Details
    int_name = 'rk4'                # Integration Algorithm (choose Function Name from Integrators.py)
    int_steps = 2                   # Number of Intermediate Integration Steps

    # -*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-

    def __init__(self,sys_instance=None,filename=None):
        assert ((sys_instance is None)!=(filename is None)), "Provide a dynamic system either by instance or by filename"
        if sys_instance is not None:
            self.sys = sys_instance
        elif filename is not None:
            system = importlib.import_module(filename)
            self.sys    = system.system_class()
        if self.sys.assertions_executed == False:
            base_system_class.test_assertions(self.sys)
        # TODO verbose solving without providing filename will cause crash bcs. filename does not exist

        # Get Information from System
        self.y0 = vstack([self.sys.output(x=self.sys.x0,u=self.sys.u0)])

        self.nx = self.sys.x0.shape[0]                # State
        self.nu = self.sys.u0.shape[0]                # Input
        self.ny = self.y0.shape[0]                    # Output 

        # Weighting
        # TODO retrieve weights from dynamic system?
        self.pQ = ones((self.ny,1))
        self.pR = ones((self.nu,1))

        self.filename = filename

        # Add a Container for All Parameters That Are Subject to Vary
        self.parameters_checked = False
        self.VPC = varpar_container(self,outside_update=self.update)

    def update(self):

            self.T_int     = self.Ts/self.int_steps        # Integration Time for Intermediate Steps
            self.nsim = int(ceil(self.sys.Tsim/self.Ts))
            
            # Get Information from System
            self.y0 = vstack([self.sys.output(x=self.sys.x0,u=self.sys.u0)])

            # Stack Optimization Variables and Corresponding Bounds
            self.optVars0        = tile(vstack([self.sys.u0, self.sys.x0 ]),(self.Hp,1))
            self.lbOptVars       = tile(vstack([self.sys.lbu,self.sys.lbx]),(self.Hp,1))
            self.ubOptVars       = tile(vstack([self.sys.ubu,self.sys.ubx]),(self.Hp,1))

            nOptVars        = (self.nx+self.nu)*self.Hp         # Nr. of Optimization Variables
            nAddConstraints = self.nx*self.Hp                   # Nr. of Multiple Shooting Constraints

            self.lambda0         = zeros((nAddConstraints,1))
            self.mu0             = zeros((nOptVars,1))

            self.check()

    def set_Ts(self,Ts):
        self.Ts = Ts
        self.VPC.update_parameter_class()

    def set_Hp(self,Hp,seconds=False):
        self.Hp = Hp
        #TODO add option to set Hp as seconds
        self.VPC.update_parameter_class()

    def set_SQP_parameters(self,nsqp,asqp,Jtol):
        nsqp = self.nsqp
        asqp = self.asqp
        Jtol = self.Jtol
        self.VPC.update_parameter_class()

    def set_integrator(self,name,steps):
        self.int_name = name
        self.int_steps = steps
        self.VPC.update_parameter_class()
    
    def set_weights(self,pQ,pR):
        self.pQ , self.pR = pQ , pR
        self.VPC.update_parameter_class()
    
    def add_varpar(self,name,values):
        self.VPC.add_varpar(ref_to_instance=self,name=name,values=values)
    
    def store_experiment(self,results,iterate=True):
        self.VPC.store_experiment(results=results,iterate=iterate)

    """
    Can print specific values stored in experiment only, e.g.
    print_experiments(names=['Ts','nsqp'])
    """
    def print_experiments(self,names=[]):
        return self.VPC.print_experiments(names=names)

    def plot_experiments(self,noise = False):
        return self.VPC.plot_experiments(noise=noise)

    def check(self):
        assert(isinstance(self.sys,base_system_class))
        assert(self.pQ.shape == (self.ny,1))
        assert(self.pR.shape == (self.nu,1))
        assert self.int_steps>0
        assert self.Hp>0
        assert self.Ts>0
        assert len(self.int_name)>0
        self.parameters_checked = True

# ------------------------------- EO CLASS parameter_class ---------------------------------------------
    