from createCasadiFunctions import create_functions
from casadi import vertcat, conic, norm_2, mtimes
import parNMPC
import numpy as np
from time import time

class optimization:
    ndigits = 3
    printLevel = ['none','low','medium','high','tabular','debug_iter']
    optionsConic = {'printLevel':printLevel[1]}

    def __init__(self,verbose=False):
        self.verbose = verbose
        pass

    def run(self,p):

        if self.verbose:
            print("Simulating ",p.filename)

        while not(p.VPC.finished):
            start_time = time()

            if self.verbose:
                print("Starting iteration with follwing parameter variation:")
                p.VPC.print_current()
            
            qp_W,qp_gradJ,qp_Ah,qp_h,FJk,FInt,FInt_real,FY = create_functions(verbose=self.verbose,p=p)
            optionsQP = {'h':qp_W.sparsity_out(0),'a':qp_Ah.sparsity_out(0)}
            S = conic('S', 'qpoases', optionsQP,self.optionsConic)

            t = np.arange(0,(p.nsim+1)*p.Ts,p.Ts)
            r0 = p.sys.reference(t[0])

            xsim = np.zeros((p.nx,p.nsim+1))
            xsim[:,[0]] = p.sys.x0
            usim = np.zeros((p.nu,p.nsim+1))
            usim[:,[0]] = p.sys.u0
            ysim = np.zeros((p.ny,p.nsim+1))
            ysim[:,[0]] = p.y0
            ymeas = np.zeros((p.ny,p.nsim+1))
            ymeas[:,[0]] = p.y0

            while t.shape[0]>p.nsim+1:
                t = t[:-1]
                
            optVars = p.optVars0
            lambda_ = p.lambda0
            mu      = p.mu0
            para = vertcat(p.sys.x0,p.sys.u0,r0,p.pQ,p.pR)

            # Time Simulation
            for i in range(1,p.nsim+1):
                if self.verbose:
                    print("SimStep: ",i,"/",p.nsim)
                W = qp_W(optVars,para,lambda_)
                gradJ = qp_gradJ(optVars,para)
                Ah    = qp_Ah(optVars,para)
                h     = qp_h(optVars,para)
        
                norm_dL = norm_2(vertcat(gradJ+mtimes(Ah.T,lambda_)+mu,h))
                k = 0
                while norm_dL > p.Jtol and k<p.nsqp:
                    lbx     = p.lbOptVars - optVars
                    ubx     = p.ubOptVars - optVars

                    sol     = S(h=W,g=gradJ,a=Ah,lba=-h,uba=-h,lbx=lbx,ubx=ubx)

                    dvar    = sol['x']
                    smu     = sol['lam_x']-mu
                    slambda = sol['lam_a']-lambda_

                    optVars = optVars+p.asqp*dvar
                    mu      = mu+ p.asqp*smu
                    lambda_ = lambda_+ p.asqp*slambda

                    W       = qp_W(optVars,para,lambda_)
                    gradJ   = qp_gradJ(optVars,para)
                    Ah      = qp_Ah(optVars,para)
                    h       = qp_h(optVars,para)
                    norm_dL = norm_2(vertcat(gradJ+mtimes(Ah.T,lambda_)+mu,h))
                    if self.verbose:
                        print("norm: ",norm_dL)

                    k       = k+1
            
                uOpt      = optVars[0:p.nu] 
                usim[:,[i]] = uOpt
                # integrate to get model response
                xi = FInt_real(xStart=xsim[:,[i-1]],u=usim[:,[i]])
                xsim[:,[i]] = xi['xEnd'].full()
                ysim[:,[i]] = FY(x=xsim[:,[i]],u=usim[:,[i]])['y'].full()
                ymeas[:,[i]]= ysim[:,[i]] + p.sys.noise()

                # derive reference
                r = p.sys.reference(t[i])

                # correct reference due to disturbance
                r = r + ysim[:,[i]] - ymeas[:,[i]]

                # shift optimal solution
                optVars = vertcat(optVars[p.nx+p.nu:],optVars[-1-p.nx-p.nu+1:])
                para = vertcat(xsim[:,[i]],usim[:,[i]],r,p.pQ,p.pR)
                lambda_ = vertcat(lambda_[p.nx:],lambda_[-1-p.nx+1:])
                mu = vertcat(mu[p.nx+p.nu:],mu[-1-p.nx-p.nu+1:])
                
            run_time = round(time()-start_time, self.ndigits)
            signals = {'usim':usim, 'xsim':xsim, 'ysim':ysim, 'ymeas':ymeas, 'time':t}
            algo_vars = {'run_time':run_time}
            results = {'signals':signals, 'algo_vars':algo_vars}
            p.store_experiment({'results':results},iterate=True)

        return usim, xsim, ysim, ymeas

