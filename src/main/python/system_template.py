
# Copy the commented class definition below in a new file.py, fill all empty vars and
# implement the functions derivative, output and noise

# from casadi import vertcat
# from system_template import base_system_class as bsc
#  class system_class(bsc):
#     # Copy constructor and system functions to deriving class and uncomment all lines
#     # It is recommended to use the casadi function vertcat for return values. This is relevant when casadi uses these functions to create to optimization routine
#     def __init__(self):
#         # define the relevant physical constants
#         # 

#         # States names and description
#         # x1: ---
#         # x2: ---
#         #  ...
#         self.state_labels = []

#         # Inputs names and description
#         # u1: ---
#         # u2: ---
#         #  ...
#         self.input_labels = []

#         # Outputs names and description
#         # y1: ---
#         #  ...
#         self.output_labels= []

#         # Initial conditions
#         self.x0 = []
#         self.u0 = []

#         # State Bounds
#         self.lbx = []
#         self.ubx = []

#         # Input Bounds
#         self.lbu = []
#         self.ubu = []

#         # Reference Trajectory
#         # make list of lists of time and data points
#         # e.g. for two outputs r_time = [[t1,t2,...],[t1',t2',...]] and
#                             #  r_data = [[d1,d2,...],[d1',d2',...]]
#         # DON'T FORGET OUTER BRACKETS []!
#         self.r_time = [[]]
#         self.r_data = [[]]
        
#         # make sure dimensions match
#         bsc.test_assertions(self)

#     # modelled system dynamics
#     def derivative_model(self,x,u):
#         # xdot = dx/dt(x,u) implementation goes here
#         dx1 = x[1]
#         dx2 = u[0]
#         return = vertcat(dx1,dx2)

# real system dynamics
#     def derivative_real(self,x,u):
#         # xdot = dx/dt(x,u) mimicking real system goes here
#         dx1 = 1.01*x[1] + noise
#         dx2 = u[0] + noise
#         return = vertcat(dx1,dx2)

#     # system output
#     def output(self,x,u):
#         return = vertcat(x[0])

#     # system-specific noise function
#     def noise(self):
#         return vertcat(0)


from casadi import vertcat
from numpy import vstack, array, interp

class base_system_class:
    assertions_executed = False

    def derivative_model(self):
        raise NotImplementedError
    
    def derivative_real(self):
        raise NotImplementedError

    def output(self):
        raise NotImplementedError

    def noise(self):
        raise NotImplementedError

    def reference(self,t):
        r = [interp(t,self.r_time[j],self.r_data[j]) for j in range(len(self.r_time))]
        return vstack(r)

    def test_assertions(self):

        # correct dimensions 
        self.x0  = vstack(self.x0)
        self.u0  = vstack(self.u0)
        self.lbx = vstack(self.lbx)
        self.ubx = vstack(self.ubx)
        self.lbu = vstack(self.lbu)
        self.ubu = vstack(self.ubu)

        # Convert to arrays and find maximum reference time
        self.r_time = [array(self.r_time[i]) for i in range(len(self.r_time))]
        self.r_data = [array(self.r_data[i]) for i in range(len(self.r_data))]
        self.Tsim = max([self.r_time[i][-1] for i in range(len(self.r_time))])

        # assert derivative dimension
        assert self.derivative_model(x=self.x0,u=self.u0).shape == self.x0.shape
        assert self.derivative_real(x=self.x0,u=self.u0).shape == self.x0.shape
        assert self.x0.shape[1]   == 1
        assert self.u0.shape[1]   == 1
        y = self.output(x=self.x0,u=self.u0)
        assert y.shape[1]         == 1
        assert self.noise().shape == y.shape
        
        # check labels
        assert len(self.state_labels) == self.x0.shape[0]
        assert len(self.input_labels) == self.u0.shape[0]
        assert len(self.output_labels)== y.shape[0]

        # check boundaries
        assert self.lbx.shape == self.x0.shape
        assert self.ubx.shape == self.x0.shape
        assert self.lbu.shape == self.u0.shape
        assert self.ubu.shape == self.u0.shape

        # check references
        assert len(self.r_time) == len(self.r_data) # TODO is this line necessary?
        for j in range(len(self.r_time)):
            assert len(self.r_time[j]) == len(self.r_data[j])
        assert self.reference(0).shape == y.shape
        for i in self.r_time:                                                               # make sure all reference times start at time 0
            assert min(i) == 0

        self.assertions_executed = True