
import Integrators
# import parNMPC as p
from casadi import MX, Function, vertcat, jacobian, hessian, dot
# import numpy as np


def create_functions(verbose,p):
    sys = p.sys
    x           = MX.sym('x',p.nx)
    u           = MX.sym('u',p.nu)
    y           = sys.output(x=x,u=u)

    # Objective Function
    r           = MX.sym('r',p.ny)
    du          = MX.sym('du',p.nu)

    pQ          = MX.sym('pQ',p.ny)
    pR          = MX.sym('pR',p.nu)

    # TODO make the cost in a seperate file, maybe system.py
    # There should be nothing to be changed here!
    # TODO alternative idea: make a fct "custom_costs(?)". parNMPC can check if existent
    # can then be included here
    J = 0
    for i in range(p.ny):
        J = J + pQ[i]*(y[i]-r[i])**2
    for i in range(p.nu):
        J = J + pR[i]*du[i]**2
    FJ = Function('FJ',[x,u,du,r,pQ,pR],[J],\
        ['x','u','du','r','pQ','pR'],['J'])

    # Get integration method specified in parameters
    if verbose:
        print("createCasadiFunctions: using integrator algorithm: ",p.int_name)
    integrate = getattr(Integrators, p.int_name)

    # Define Variables
    x0          = MX.sym('x0',p.nx,1)
    u           = MX.sym('u',p.nu,1)
    # Create Casadi Function to Compute State Evolution for One Time Step
    FInt        = Function('FInt',\
        [x0,u], [integrate(x0=x0,u=u,Fxdot=sys.derivative_model,T_int=p.T_int,int_steps=p.int_steps)],\
        ['xStart','u',],['xEnd'])
    FInt_real        = Function('FInt',\
        [x0,u], [integrate(x0=x0,u=u,Fxdot=sys.derivative_real,T_int=p.T_int,int_steps=p.int_steps)],\
        ['xStart','u',],['xEnd'])

    # Create Casadi Function to Compute Output
    FY          = Function('FY',[x,u], [sys.output(x=x,u=u)],['x','u'],['y'])

    # NLP Formulation
    optVars     = []
    h           = []
    Jk          = 0

    # Define OptVars
    Uk          = MX.sym('Uk',p.nu,1,p.Hp)
    Sk          = MX.sym('Sk',p.nx,1,p.Hp)

    # opt_p = [x0;u0;r;Q1;Q2;...;R1;R2;...]
    opt_p       = MX.sym('opt_p',p.nx+p.nu+p.ny+p.ny+p.nu)
    # Indices for Convinience
    ind_x       = range(0,p.nx)
    ind_u       = range(ind_x[-1]+1,ind_x[-1]+1+p.nu)
    ind_r       = range(ind_u[-1]+1,ind_u[-1]+1+p.ny)
    ind_Q       = range(ind_r[-1]+1,ind_r[-1]+1+p.ny)
    ind_R       = range(ind_Q[-1]+1,ind_Q[-1]+1+p.nu)


    for k in range(p.Hp):
        optVars = vertcat(optVars,Uk[k],Sk[k])

        if k==0:
            Fk = FInt(xStart=opt_p[ind_x],u=Uk[k])
            
        else:
            Fk = FInt(xStart=Sk[k-1],u=Uk[k])

        XkEnd = Fk['xEnd']
        
        # Multiple Shooting Equality Constraint
        h       = vertcat(h,XkEnd-Sk[k])

        # Add Cost for Current Interval to Total Cost
        if k==0:
            Jk  = FJ(x=Sk[k],u=Uk[k],du=Uk[k]-opt_p[ind_u],\
                r=opt_p[ind_r],pQ=opt_p[ind_Q],pR=opt_p[ind_R])['J']
        else:
            Jk  = Jk + FJ(x=Sk[k],u=Uk[k],du=Uk[k]-Uk[k-1],\
                r=opt_p[ind_r],pQ=opt_p[ind_Q],pR=opt_p[ind_R])['J']

    FJk         = Function('FJK',[optVars,opt_p],[Jk],['optVars','opt_p'],['Jk'])

    assert h.shape == (p.nx*p.Hp,1)
    lambda_ = MX.sym('lambda',p.nx*p.Hp)
    # mu = 
    L           = Jk + dot(lambda_,h)

    [W,_]       = hessian(L,optVars)
    gradJ       = jacobian(Jk,optVars).T
    Ah          = jacobian(h,optVars)

    qp_W        = Function('qp_W',[optVars,opt_p,lambda_],[W],['optVars','opt_p','lambda_'],['W'])
    qp_gradJ    = Function('qp_gradJ',[optVars,opt_p],[gradJ],['optVars','opt_p'],['grad_J'])
    qp_Ah       = Function('qp_Ah',[optVars,opt_p],[Ah],['optVars','opt_p'],['Ah'])
    qp_h        = Function('qp_h',[optVars,opt_p],[h],['optVars','opt_p'],['h'])
    return qp_W,qp_gradJ,qp_Ah,qp_h,FJk,FInt,FInt_real,FY
    # -------------------------------------------

    # x = np.vstack([0, .5])
    # for _ in range(100):
    #     x = FInt(xStart=x,u=-0.1)
    #     x = x['xEnd']
    #     print(x)

