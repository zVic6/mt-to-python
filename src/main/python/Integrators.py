# Package with various Integrators for comparison
# 
# Adding a custom integrator should follow the parameter naming i.e.
# integrator_name(x0,u,Fxdot,T_int,int_steps)
# - x0          : initial state
# - u           : input
# - Fxdot       : system dynamics i.e. d/dt(x) = Fxdot(x,u)
# - T_int       : integration step
# - int_steps   : number of integration steps

# Runge Kutta 4 Algorithm --------------------------------
def rk4(x0,u,Fxdot,T_int,int_steps):
    XEnd = x0
    for _ in range(int_steps):
        k1X = Fxdot(x=XEnd, u=u)
        k2X = Fxdot(x=XEnd + T_int/2 * k1X , u=u)
        k3X = Fxdot(x=XEnd + T_int/2 * k2X , u=u)
        k4X = Fxdot(x=XEnd + T_int   * k3X , u=u)
        XEnd = XEnd + T_int/6 * (k1X + 2*k2X + 2*k3X + k4X)
 
    return XEnd

# Euler Algorithm ----------------------------------------
def euler(x0,u,Fxdot,T_int,int_steps):
    XEnd = x0
    for _ in range(int_steps):
        XEnd = XEnd + Fxdot(x=XEnd , u=u)*T_int

    return XEnd

# Midpoint Method -----------------------------------------
def midpoint(x0,u,Fxdot,T_int,int_steps):
    XEnd = x0
    for _ in range(int_steps):
        XEnd = XEnd + Fxdot(x=Fxdot(x=XEnd,u=u)*T_int/2 , u=u)*T_int

    return XEnd
    
# optional TODO: function that takes butcher tableau and returns corresponding integrator
# ...