import unittest
# from casadi import vertcat
from numpy import vstack
import os
import Integrators

def Fxdot(x,u):
    return vstack([x[1],u])

class IntegratorTest(unittest.TestCase):
    
    # Get all integration functions in Integrators
    int_fcts = [getattr(Integrators,fct_name) for fct_name in dir(Integrators) if callable(getattr(Integrators, fct_name))]
    
    def testNegativeInput(self):
        x0 = vstack([1,0])
        u = -1
        T_int = 0.1
        int_steps = 4

        for int_fct in self.int_fcts:
            print("Testing ",int_fct)
            xEnd = int_fct(x0,u,Fxdot,T_int,int_steps)
            self.assertGreater(x0[0],xEnd[0])
            self.assertGreater(x0[1],xEnd[1])

    def testPositiveInput(self):
        x0 = vstack([1,0])
        u = 1
        T_int = 0.1
        int_steps = 4

        for int_fct in self.int_fcts:
            xEnd = int_fct(x0,u,Fxdot,T_int,int_steps)
            self.assertLess(x0[0],xEnd[0])
            self.assertLess(x0[1],xEnd[1])
