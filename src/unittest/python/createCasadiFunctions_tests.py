import unittest

from test_system import system_class as sys
from parNMPC import parameter_class
import createCasadiFunctions as ccf

class CCFTest(unittest.TestCase):
    p = parameter_class(sys_instance=sys())
    # check execution runs without problems
    def test_ccf(self):
        ccf.create_functions(verbose=False,p=self.p)