import unittest
from system_template import base_system_class
class system_template_Test(unittest.TestCase):
    def test_derived_but_not_implemented(self):
        class A(base_system_class):
            pass
        a = A()
        self.assertRaises(NotImplementedError,a.derivative_model)
        self.assertRaises(NotImplementedError,a.derivative_real)
        self.assertRaises(NotImplementedError,a.output)
        self.assertRaises(NotImplementedError,a.noise)