import unittest

import parNMPC
import optimization

class optimizationTest(unittest.TestCase):
    
    p   = parNMPC.parameter_class(filename='test_system')
    
    from test_system import system_class as sys
    p   = parNMPC.parameter_class(sys_instance=sys())
    
    p.add_varpar(name="Ts",values=[0.1,1])
    
    mpc = optimization.optimization()
    mpc.run(p)
