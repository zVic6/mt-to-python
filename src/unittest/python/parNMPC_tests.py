import unittest

from test_system import system_class as sys
from parNMPC import parameter_class
import createCasadiFunctions as ccf

class ParNMPCTest(unittest.TestCase):
    p = parameter_class(sys_instance=sys())
    """ Test assertions completed """
    def test_assertionsCompleted(self):
        self.assertTrue(self.p.sys.assertions_executed)

    def test_parameter_class_check(self):
        self.assertTrue(self.p.parameters_checked)