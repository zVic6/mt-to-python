Refer to the Wiki for further information.

This project was developed in a default-settings pybuilder==0.11.17 virutal environment

For quick testing convenience I refrained from using the recommended way of
    - building the project
    - run $ pip install target/dist/NMPC-2.0/dist/NMPC-2.0.tar.gz
    - add shebang to main.py
    - run $ main.py
    
Future updates will include:
- Corrected unittesting (not updated since major changes to master in commit f522a1cb77bea848e7fa9bf190ae0d154f2283b3);
- OOP improvements;
- Further code documentation;


            |
            |
            V

************************************************************
  For a demonstration showing the step response control to a
  double integrator with bounded inputs do the following

Clone the Git repository.
In an Anaconda prompt with admin rights type

    conda create -n env python=3.6
    conda activate env
    pip install -r requirements.txt
    cd src\main\scripts
    python main.py

Alternatively you can use virutalenv instead of conda

************************************************************
